#SingleInstance force
CoordMode, Pixel, Screen ; Using Window search
CoordMode, Mouse, Screen



Pause::
ExitApp
Suspend
Pause,,1
SetTimer, ManaSit, off
return

^=::Reload ; Assign a hotkey to restart the script.


clearedServerLog = 0
stuck = 0
atChest = 0
stuckTimes = 0

flagSpot:
PixelSearch, Px, Py, %skullX1%, %skullY1%, %skullX2%, %skullY2%, 0x9f9b9b, 3, RGB
if ErrorLevel = 0
{
	MouseMove, %Px%, %Py%
	MouseClick, L
	;ToolTip, going to flag spot
	sleep 12000
	return
}
return


skullSpot:
PixelSearch, Px, Py, %skullX1%, %skullY1%, %skullX2%, %skullY2%, 0x9f9b9b, 3, RGB
if ErrorLevel = 0
{
	MouseMove, %Px%, %Py%
	MouseClick, L
	;ToolTip, going to skull spot
	sleep 12000
	return
}
return

starSpot:
PixelSearch, Px, Py, %starX1%, %starY1%, %starX2%, %starY2%, 0xf7941c, 3, RGB
if ErrorLevel = 0
{
	MouseMove, %Px%, %Py%
	MouseClick, L
	;ToolTip, going to star spot
	sleep 12000
	GoSub, gotoSpears
	if atSpears = 0
	{
		MouseMove, %Px%, %Py%
		MouseClick, L
		sleep 5000
	}
	return
}
return


^u::
MouseMove, %whitetxtX2%, %whitetxtY2%
return

checkWhiteText:
;ToolTip, checking white text
PixelSearch, Px, Py, %whitetxtX1%, %whitetxtY1%, %whitetxtX2%, %whitetxtY2%, 0xF0F0F0, 3, RGB
if ErrorLevel = 0
{
	stuck = 0
	;ToolTip, found white text
}
else
{
	stuck = 1
	;ToolTip, no white text
}
return

clearServerLog:
MouseMove, %servlogX%, %servlogY%
MouseClick, R
sleep 500
MouseMove, 39, 57, 50, R
sleep 500
MouseClick, L
sleep 500
MouseClick, L
sleep 500
clearedServerLog = 1
;send, {escape}
return


checkStuckTimer:
GoSub, starSpot
GoSub, grabSpears
stuck = 0
return

routine:
Loop
{
	if stuck = 1
	{
		sleep 2000
		continue
	}
	GoSub, starSpot
	GoSub, grabSpears
	GoSub, killCreatures
	GoSub, grabSpears
	GoSub, skullSpot
	GoSub, killCreatures

	GoSub, starSpot
	GoSub, grabSpears
	GoSub, killCreatures
	GoSub, grabSpears

	GoSub, flagSpot
	GoSub, killCreatures

	sleep 1000
}
return

gotoSpears:
PixelSearch, Px, Py, %crateX1%, %crateY1%, %crateX2%, %crateY2%, 0xe9b27b, 3, RGB
if ErrorLevel = 0
{
	atSpears = 1
}
else
{
	atSpears = 0
}
return

grabSpears:
PixelSearch, Px, Py, %crateX1%, %crateY1%, %crateX2%, %crateY2%, 0xe9b27b, 3, RGB
if ErrorLevel = 0
{
	atSpears = 1
	MouseClick, R, %Px%, %Py%
	sleep 1000
}
else
{
	atSpears = 0
}
return

killCreatures:
Loop
{
	PixelSearch, Px, Py, %battleX1%, %battleY1%, %battleX2%, %battleY2%, 0x000000, 3, RGB
	if ErrorLevel = 0
	{
		PixelSearch, Px, Py, %battleX1%, %battleY1%, %battleX2%, %battleY2%, 0xFF0000, 3, RGB
		if ErrorLevel <> 0
		{
			send, {space}
		}
	}
	else
		return
	sleep 2000
}
return

; green shield: 
; 1875, 175

setAllCoords:
PixelSearch, Px, Py, 0, 0, 2000, 2000, 0x269D26, 1, RGB
if ErrorLevel = 0
{
	shieldX = %Px%
	shieldY = %Py%

	servlogX = 160
	servlogY = 974

	starX1 = %shieldX%
	starY1 = %shieldY%
	starX1 -= 125
	starY1 -= 165
	starX2 = %starX1%
	starY2 = %starY1%
	starX2 += 100
	starY2 += 100

	skullX1 = %shieldX%
	skullY1 = %shieldY%
	skullX1 -= 125
	skullY1 -= 165
	skullX2 = %skullX1%
	skullY2 = %skullY1%
	skullX2 += 100
	skullY2 += 100
	
	battleX1 = %shieldX%
	battleY1 = %shieldY%
	battleX1 -= 303
	battleY1 -= 102
	battleX2 = %battleX1%
	battleY2 = %battleY1%
	battleX2 += 143
	battleY2 += 224

	whitetxtX1 = %shieldX%
	whitetxtY1 = %shieldY%
	whitetxtX1 -= 1567
	whitetxtY1 += 814
	whitetxtX2 = %whitetxtX1%
	whitetxtY2 = %whitetxtY1%
	whitetxtX2 += 60
	whitetxtY2 += 50

	crateX1 = %shieldX%
	crateY1 = %shieldY%
	crateX1 -= 1153
	crateY1 += 204
	crateX2 = %crateX1%
	crateY2 = %crateY1%
	crateX2 += 36
	crateY2 += 35
	
	;sleep 500
	;MouseMove, 10, 10, , R
	;sleep 500
	;MouseMove, %shieldX%, %shieldY%

	;sleep 500
	;MouseMove, %manaX1%, %manaY1%
	;sleep 500
	;MouseMove, %manaX2%, %manaY2%
}
return


^b::
GoSub, setAllCoords
return

^h::
GoSub, routine
return

^j::
if toggle_get_stuck = 1
{
	toggle_get_stuck = 0
	SetTimer, checkStuckTimer, off
	;ToolTip, get stuck: off 
}
else
{
	toggle_get_stuck = 1
	SetTimer, checkStuckTimer, 120000
	;ToolTip, get stuck: on
}
return

