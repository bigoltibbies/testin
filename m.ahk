#SingleInstance force
CoordMode, Pixel, Screen ; Using Window search
CoordMode, Mouse, Screen

Pause::
ExitApp
Suspend
Pause,,1
SetTimer, Spears, off
return

^=::Reload ; Assign a hotkey to restart the script.

ManaSit:
PixelSearch, Px, Py, 1785, 299, 1800, 311, 0x5350DA, 3, RGB
if ErrorLevel = 0
{
	send, {b}
	SetTimer, ManaSit, 1000
	ToolTip, sending
}
else
{
	SetTimer, ManaSit, 58000
	send, {k}
	ToolTip, not sending
}
return

;SetTimer, ManaSit, 2000


; spears
^h::
if toggle_spears = 1
{
	toggle_spears = 0
	SetTimer, ManaSit, off
	ToolTip, ManaSit: off 
}
else
{
	toggle_spears = 1
	SetTimer, ManaSit, 2000
	ToolTip, ManaSit: on
}
return