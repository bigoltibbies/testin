#SingleInstance force
CoordMode, Pixel, Screen ; Using Window search
CoordMode, Mouse, Screen
MouseMove, 1151, 651
sleep 500

Pause::
ExitApp
Suspend
Pause,,1
SetTimer, Spears, off
return

^=::Reload ; Assign a hotkey to restart the script.

; every 60 secs, check for white text then clear server log, if there is no white text, it means i am stuck. Go get some spears. If there is white text, clear server log and wait another 5-60 seconds. 
; procedure: click map spot, loop until no creatures, click next spot, go to spears chest, start again
; when need to go get spears, set flag to do that gogetspears = 1 so attacking code can return

Spears:
;SetTimer, Spears, on
PixelSearch, Px, Py, 697, 376, 750, 417, 0xe9b27b, 3, RGB
if ErrorLevel = 0
{
	MouseClick, R, 738, 409
	SetTimer, Spears, 58000
}
else
{
	SetTimer, Spears, 2000
}
return

; spears
^h::
if toggle_spears = 1
{
	toggle_spears = 0
	SetTimer, Spears, off
	ToolTip, Spears: off 
}
else
{
	toggle_spears = 1
	SetTimer, Spears, 2000
	ToolTip, Spears: on
}
return


NoCreatures:
PixelSearch, Px, Py, 1572, 70, 1704, 309, 0x000000, 3, RGB
if ErrorLevel = 0
{
	ToolTip, a`ncreature
	PixelSearch, Px, Py, 1560, 9, 1732, 150, 0xFF0000, 3, RGB
	if ErrorLevel <> 0
	{
		;MouseMove, %Px%, %Py%
		send, {space}
	}
}
else
{
	ToolTip, no creatures
	;MsgBox, NO creatures
	PixelSearch, Px, Py, 1764, 72, 1780, 90, 0x9f9b9b, 3, RGB
	if ErrorLevel = 0
	{
		MouseMove, %Px%, %Py%
		MouseClick, L
		sleep 15000
		;send, {space}
		return
	}
	PixelSearch, Px, Py, 1820, 35, 1836, 55, 0xf7941c, 3, RGB
	if ErrorLevel = 0
	{
		MouseMove, %Px%, %Py%
		MouseClick, L
		sleep 15000
		;send, {space}
		return
	}
}
return

; NoCreatures
^j::
if no_creatures = 1
{
	no_creatures = 0
	SetTimer, NoCreatures, off
	ToolTip, NoCreatures: off 
}
else
{
	no_creatures = 1
	SetTimer, NoCreatures, 3000
	ToolTip, NoCreatures: on
}
return


Attack:
PixelSearch, Px, Py, 1560, 9, 1732, 150, 0xFF0000, 3, RGB
if ErrorLevel <> 0
{
	;MouseMove, %Px%, %Py%
	send, {space}
}
sleep 2000
return

^g::
if toggle_attack = 1
{
	toggle_attack = 0
	SetTimer, Attack, off
	ToolTip, Attack: off 
}
else
{
	toggle_attack = 1
	SetTimer, Attack, 2000
	ToolTip, Attack: on
}
return



; main loop
^f::
Loop 
{
	;; attack
	PixelSearch, Px, Py, 1560, 9, 1732, 150, 0xFF0000, 3, RGB
	if ErrorLevel <> 0
	{
		;MouseMove, %Px%, %Py%
		send, {space}
	}
	sleep 2000
}
return

; Example #3: Detection of single, double, and triple-presses of a hotkey. This
; allows a hotkey to perform a different operation depending on how many times
; you press it:
#c::
if winc_presses > 0 ; SetTimer already started, so we log the keypress instead.
{
	winc_presses += 1
	return
}
; Otherwise, this is the first press of a new series. Set count to 1 and start
; the timer:
winc_presses = 1
SetTimer, KeyWinC, 400 ; Wait for more presses within a 400 millisecond window.
return

KeyWinC:
SetTimer, KeyWinC, off
if winc_presses = 1 ; The key was pressed once.
{
	Run, m:\  ; Open a folder.
}
else if winc_presses = 2 ; The key was pressed twice.
{
	Run, m:\multimedia  ; Open a different folder.
}
else if winc_presses > 2
{
	MsgBox, Three or more clicks detected.
}
; Regardless of which action above was triggered, reset the count to
; prepare for the next series of presses:
winc_presses = 0
return


