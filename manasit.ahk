#SingleInstance force
CoordMode, Pixel, Screen ; Using Window search
CoordMode, Mouse, Screen



Pause::
ExitApp
Suspend
Pause,,1
SetTimer, ManaSit, off
return

^=::Reload ; Assign a hotkey to restart the script.


^l::
Gosub, LifeRings
return


ROH:
PixelSearch, Px, Py, %RohX1%, %RohY1%, %RohX2%, %RohY2%, 0xF47160, 3, RGB
if ErrorLevel = 0
{
	PixelSearch, Px, Py, %wearingLifeRingX1%, %wearingLifeRingY1%, %wearingLifeRingX2%, %wearingLifeRingY2%, 0xFDBCAB, 3, RGB
	if ErrorLevel <> 0
	{
		send, {o}
		ToolTip, equipped roh
	}
}
return

LifeRings:
PixelSearch, Px, Py, %lifeX1%, %lifeY1%, %lifeX2%, %lifeY2%, 0x4CA83A, 3, RGB
if ErrorLevel = 0
{
	PixelSearch, Px, Py, %wearingLifeRingX1%, %wearingLifeRingY1%, %wearingLifeRingX2%, %wearingLifeRingY2%, 0x5BD543, 3, RGB
	if ErrorLevel <> 0
	{
		send, {l}
		ToolTip, equipped life ring
	}
}
return

ManaSit:
;PixelSearch, Px, Py, 1785, 299, 1800, 311, 0x5350DA, 3, RGB
PixelSearch, Px, Py, %manaX1%, %manaY1%, %manaX2%, %manaY2%, 0x3633A7, 3, RGB
if ErrorLevel = 0
{
	send, {b}
	SetTimer, ManaSit, 1000
	ToolTip, sending
}
else
{
	SetTimer, ManaSit, 38000
	send, {k}
	ToolTip, not sending
}
GoSub, LifeRings
GoSub, ROH
return


^b::
GoSub, SetAllCoords
return

; green shield: 
; 1875, 175

SetAllCoords:
PixelSearch, Px, Py, 0, 0, 2000, 2000, 0x269D26, 1, RGB
if ErrorLevel = 0
{
	MouseMove, %Px%, %Py%
	shieldX = %Px%
	shieldY = %Py%

	manaX1 = %Px%
	manaX2 = %Px%
	manaY1 = %Py%
	manaY2 = %Py%

	manaX1 -= 102
	manaX2 -= 23
	manaY1 += 115
	manaY2 += 130

	lifeX1 = %shieldX%
	lifeX2 = %shieldX%
	lifeY1 = %shieldY%
	lifeY2 = %shieldY%

	lifeX1 -= 516
	lifeX2 -= 491
	lifeY1 -= 92
	lifeY2 -= 66

	wearingLifeRingX1 = %shieldX%
	wearingLifeRingX2 = %shieldX%
	wearingLifeRingY1 = %shieldY%
	wearingLifeRingY2 = %shieldY%

	wearingLifeRingX1 -= 120
	wearingLifeRingX2 -= 102
	wearingLifeRingY1 += 40
	wearingLifeRingY2 += 60

	RohX1 = %shieldX%
	RohX2 = %shieldX%
	RohY1 = %shieldY%
	RohY2 = %shieldY%

	RohX1 -= 516
	RohX2 -= 491
	RohY1 += 16
	RohY2 += 41

	;sleep 500
	;MouseMove, 10, 10, , R
	;sleep 500
	;MouseMove, %shieldX%, %shieldY%

	;sleep 500
	;MouseMove, %manaX1%, %manaY1%
	;sleep 500
	;MouseMove, %manaX2%, %manaY2%
}
return

; spears
^h::
if toggle_spears = 1
{
	toggle_spears = 0
	SetTimer, ManaSit, off
	ToolTip, ManaSit: off 
}
else
{
	toggle_spears = 1
	SetTimer, ManaSit, 2000
	ToolTip, ManaSit: on
}
return
